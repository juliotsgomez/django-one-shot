from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {"detail_object": detail}
    return render(request, "todos/detail.html", context)


def todo_list_list(request):
    todo_list_lists = TodoList.objects.all()
    context = {"todo_list_list": todo_list_lists}
    return render(request, "todos/list.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.Post)
        if form.is_valid():
            form.save()
            return redirect("todos_list_detail")
    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list_lists = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list_lists)
        if form.is_valid():
            todo_list_lists = form.save()
            return redirect("todo_list_detail", id=todo_lists_lists.id)
    else:
        form = TodoListForm(instance=todo_list_lists)
        context = {
            "todo_list_lists": todo_list_lists,
            "form": form,
        }
        return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list_lists = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_lists.delete()
        return redirect("todo_list_list")

    return render(request, "todo_list_lists/delete.html")
